package com.homework.goit.company;

import com.homework.goit.common.DataAccessObject;

public interface CompanyDAO extends DataAccessObject<Company> {
}
