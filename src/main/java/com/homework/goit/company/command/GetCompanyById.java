package com.homework.goit.company.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAO;

public class GetCompanyById implements Command {
    private View view;
    private CompanyDAO companyDAO;

    public GetCompanyById(View view, CompanyDAO companyDAO) {
        this.view = view;
        this.companyDAO = companyDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_COMPANY_BY_ID;
    }

    @Override
    public void execute() {
        view.write("Enter company id");
        int id = Utilita.validateNumber(view.read());
        view.write("Searching company...");
        Company company = companyDAO.get(id);
        if (company != null) {
            view.write(String.format("Company(%s):\n\t%s  %s", company.getId(), company.getName(),
                    company.getLocation()));
        } else {
            view.write("Company with such id were not found!");
        }
    }
}
