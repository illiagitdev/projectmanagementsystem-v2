package com.homework.goit.company.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAO;

import java.util.List;

public class GetAllCompanies implements Command {
    private View view;
    private CompanyDAO companyDAO;

    public GetAllCompanies(View view, CompanyDAO companyDAO) {
        this.view = view;
        this.companyDAO = companyDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_ALL_COMPANIES;
    }

    @Override
    public void execute() {
        view.write("Retrieving all companies...");
        List<Company> companies = companyDAO.getAll();
        if (!companies.isEmpty()) {
            for (Company com : companies) {
                view.write(String.format("Company(%s)\n\t%s, location %s", com.getId(), com.getName(), com.getLocation()));
            }
        } else {
            view.write("No company found.");
        }
    }
}
