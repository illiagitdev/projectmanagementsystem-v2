package com.homework.goit.company.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.company.CompanyDAO;

public class DeleteCompany implements Command {
    private View view;
    private CompanyDAO companyDAO;

    public DeleteCompany(View view, CompanyDAO companyDAO) {
        this.view = view;
        this.companyDAO = companyDAO;
    }

    @Override
    public String command() {
        return Commands.DELETE_COMPANY;
    }

    @Override
    public void execute() {
        view.write("Enter company id for deleting");
        int id = Utilita.validateNumber(view.read());
        view.write("Deleting companies...");
        companyDAO.delete(id);
    }
}
