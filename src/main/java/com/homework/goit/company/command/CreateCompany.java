package com.homework.goit.company.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAO;

public class CreateCompany implements Command {
    private View view;
    private CompanyDAO companyDAO;

    public CreateCompany(View view, CompanyDAO companyDAO) {
        this.view = view;
        this.companyDAO = companyDAO;
    }

    @Override
    public String command() {
        return Commands.CREATE_COMPANY;
    }

    @Override
    public void execute() {
        view.write("Enter company name");
        String name = Utilita.validate(view.read());
        view.write("Enter company location");
        String location = view.read();
        Company company = new Company();
        company.setName(name);
        company.setLocation(location);
        view.write("Creating new company...");
        companyDAO.create(company);
    }
}
