package com.homework.goit.company.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAO;

public class UpdateCompany implements Command {
    private View view;
    private CompanyDAO companyDAO;

    public UpdateCompany(View view, CompanyDAO companyDAO) {
        this.view = view;
        this.companyDAO = companyDAO;
    }

    @Override
    public String command() {
        return Commands.UPDATE_COMPANY;
    }

    @Override
    public void execute() {
        view.write("Enter company id for update");
        int id = Utilita.validateNumber(view.read());
        view.write("Update company name");
        String name = Utilita.validate(view.read());
        view.write("Update company location");
        String location = view.read();
        Company company = new Company();
        company.setId(id);
        company.setName(name);
        company.setLocation(location);
        view.write("Updating company...");
        companyDAO.update(company);
    }
}
