package com.homework.goit.company;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class CompanyDAOImpl implements CompanyDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(CompanyDAOImpl.class);
    private SessionFactory sessionFactory;

    public CompanyDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Company company) {
        LOG.info(String.format("create: company='%s'", company.getName()));
        try (final Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(company);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("create(company):error with '%s'", company.getName()), e);
        }
    }

    @Override
    public void update(Company company) {
        LOG.info(String.format("update: company='%s'", company.getName()));
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            session.update(company);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("update(company id):error with '%s'", company.getId()), e);
        }
    }

    @Override
    public Company get(int id) {
        LOG.info(String.format("get: id='%d'", id));
        Company company = null;
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            company = session.createQuery("from Company c where c.id=:id", Company.class)
                    .setParameter("id", id)
                    .uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("get(id):error with id '%s'", id), e);
        }
        return company;
    }

    @Override
    public List<Company> getAll() {
        List<Company> companyList = new ArrayList<>();
        try (Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            companyList = session.createQuery("from Company ", Company.class).list();
            transaction.commit();
        } catch (Exception e) {
            LOG.error("getAll exception", e);
        }
        return companyList;
    }

    @Override
    public void delete(int id) {
        LOG.info(String.format("delete: id='%d'", id));
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            session.createSQLQuery("DELETE FROM companies_projects WHERE company_id=:id")
                    .setParameter("id", id).executeUpdate();
             session.createSQLQuery("DELETE FROM developers_companies WHERE company_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createQuery("DELETE FROM Company WHERE id=:id").setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("delete(id):error with id '%s'", id), e);
        }
    }
}
