package com.homework.goit.company;

import com.homework.goit.common.Entity;

import java.util.Objects;

public class Company extends Entity {
    private String name;
    private String location;

    public Company() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Company company = (Company) o;
        return Objects.equals(name, company.name) &&
                Objects.equals(location, company.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, location);
    }
}
