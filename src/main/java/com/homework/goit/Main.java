package com.homework.goit;

import com.homework.goit.common.Console;
import com.homework.goit.common.MainController;
import com.homework.goit.common.View;
import com.homework.goit.config.HibernateDatabaseConnector;

public class Main {
    public static void main(String[] args) {
        View view = new Console();
        HibernateDatabaseConnector instance = HibernateDatabaseConnector.getInstance();
        MainController controller = new MainController(view, instance.getSessionFactory());
        controller.process();
    }
}
