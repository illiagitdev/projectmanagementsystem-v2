package com.homework.goit.developer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.*;

public class DeveloperDAOImpl implements DeveloperDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(DeveloperDAOImpl.class);
    private SessionFactory sessionFactory;

    public DeveloperDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Developer developer) {
        LOG.info(String.format("create: developer='%s'", developer.getFirstName()));
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            session.save(developer);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("create(developer):error with '%s'", developer.getFirstName()), e);
        }
    }

    @Override
    public void update(Developer developer) {
        LOG.info(String.format("update: developer='%s'", developer.getFirstName()));
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            session.update(developer);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("update(developer):error with '%s'", developer.getFirstName()), e);
        }
    }

    @Override
    public Developer get(int id) {
        LOG.info(String.format("get: id='%d'", id));
        Developer developer = null;
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            developer = session.createQuery("from Developer d where d.id=:id", Developer.class)
                    .setParameter("id", id)
                    .getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("get(id):error with id '%s'", id), e);
        }
        return developer;
    }

    @Override
    public List<Developer> getAll() {
        List<Developer> developers = new LinkedList<>();
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            developers = session.createQuery("from Developer ", Developer.class).getResultList();
            transaction.commit();
        } catch (Exception e) {
            LOG.error("getAll:error -> ", e);
        }
        return developers;
    }

    @Override
    public void delete(int id) {
        LOG.info(String.format("delete: id='%d'", id));
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            session.createSQLQuery("DELETE FROM developers_skills WHERE developer_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createSQLQuery("DELETE FROM developers_companies WHERE developer_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createSQLQuery("DELETE FROM developers_projects WHERE developer_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createQuery("DELETE FROM Developer WHERE id=:id").setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("delete(id):error with id '%s'", id), e);
        }
    }
}
