package com.homework.goit.developer.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;

public class GetDeveloperById implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public GetDeveloperById(View view, DeveloperDAOImpl developerDAO) {
        this.view = view;
        this.developerDAO = developerDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_DEVELOPER_BY_ID;
    }

    @Override
    public void execute() {
        view.write("Enter id developer to search");
        int id = Utilita.validateNumber(view.read());
        view.write("Searching for developer...");
        Developer developer = developerDAO.get(id);
        if (developer != null) {
            view.write(String.format("Developer:\n\t%s %s %s %s %s", developer.getFirstName(), developer.getLastName(),
                    developer.getAge(), developer.getEmail(), developer.getGender()));
        } else {
            view.write("Developer with such id were not found!");
        }
    }
}
