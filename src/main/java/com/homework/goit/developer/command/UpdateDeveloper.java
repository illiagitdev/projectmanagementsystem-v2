package com.homework.goit.developer.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;
import com.homework.goit.developer.Gender;

import java.util.Optional;

public class UpdateDeveloper implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public UpdateDeveloper(View view, DeveloperDAOImpl developerDAO) {
        this.view = view;
        this.developerDAO = developerDAO;
    }

    @Override
    public String command() {
        return Commands.UPDATE_DEVELOPER;
    }

    @Override
    public void execute() {
        view.write("Enter id developer for update");
        int id = Utilita.validateNumber(view.read());
        view.write("Update developer first name");
        String firstName = Utilita.validate(view.read());
        view.write("Update developer last name");
        String lastName = Utilita.validate(view.read());
        view.write("Update developer age(>0)");
        int age = Utilita.validateNumber(view.read());
        view.write("Update developer email");
        String email = Utilita.validate(view.read());
        view.write("Update developer sex(MALE, FEMALE)");
        String sex = Utilita.validate(view.read());
        view.write("Update developer salary");
        int salary = Utilita.validateNumber(view.read());
        Developer developer = new Developer();
        developer.setId(id);
        developer.setFirstName(firstName);
        developer.setLastName(lastName);
        developer.setAge(age);
        developer.setEmail(email);
        Optional<Gender> tmp = Gender.getGenderValue(sex.toLowerCase());
        Gender gender = tmp.isEmpty() ? Gender.OTHER : tmp.get();
        developer.setGender(gender);
        developer.setSalary(salary);
        view.write("Updating developer...");
        try {
            developerDAO.update(developer);
        } catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }
}
