package com.homework.goit.developer.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;

import java.util.List;

public class GetAllDevelopers implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public GetAllDevelopers(View view, DeveloperDAOImpl developerDAO) {
        this.view = view;
        this.developerDAO = developerDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_ALL_DEVELOPERS;
    }

    @Override
    public void execute() {
        view.write("Retrieving all developers...");
        List<Developer> developers = developerDAO.getAll();
        if (!developers.isEmpty()) {
            for (Developer developer : developers) {
                view.write(String.format("Developer(%s):\n\t%s %s %s %s %s", developer.getId(), developer.getFirstName(),
                        developer.getLastName(), developer.getAge(), developer.getEmail(), developer.getGender()));
            }
        } else {
            view.write("No developers found.");
        }
    }
}
