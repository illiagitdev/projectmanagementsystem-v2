package com.homework.goit.developer;

import com.homework.goit.common.DataAccessObject;

public interface DeveloperDAO extends DataAccessObject<Developer> {
}
