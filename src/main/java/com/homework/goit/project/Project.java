package com.homework.goit.project;

import com.homework.goit.common.Entity;
import com.homework.goit.developer.Developer;

import java.sql.Timestamp;
import java.util.*;

public class Project extends Entity {
    private String name;
    private Timestamp releaseDate;
    private int cost;
    private Timestamp  projectStart;
    private List<Developer> developers = new ArrayList<>();

    public Project() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Timestamp releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Timestamp getProjectStart() {
        return projectStart;
    }

    public void setProjectStart(Timestamp projectStart) {
        this.projectStart = projectStart;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Project project = (Project) o;
        return Objects.equals(name, project.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }
}
