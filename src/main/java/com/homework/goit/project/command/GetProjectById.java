package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.project.Project;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

public class GetProjectById implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public GetProjectById(View view, ProjectDAOImpl projectDAO) {
        this.view = view;
        this.projectDAO = projectDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_PROJECT_BY_ID;
    }

    @Override
    public void execute() {
        view.write("Enter project id");
        int id = Utilita.validateNumber(view.read());
        view.write("Searching project...");
        Project project = projectDAO.get(id);
        if(project != null){
            view.write(String.format("Project:\n\t%s - %s $ - %s", project.getName(), project.getCost(),
                    project.getProjectStart()));
        }else {
            view.write("Project with such id were not found!");
        }
    }
}
