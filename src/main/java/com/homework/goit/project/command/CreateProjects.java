package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.project.Project;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

import java.sql.Timestamp;

public class CreateProjects implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public CreateProjects(View view, ProjectDAOImpl projectDAO) {
        this.view = view;
        this.projectDAO = projectDAO;
    }

    @Override
    public String command() {
        return Commands.CREATE_PROJECT;
    }

    @Override
    public void execute() {
        view.write("Enter project name");
        String name = Utilita.validate(view.read());
        view.write("Enter release date (format YYYY-MM-DD)");
        Timestamp releaseDate = Utilita.validateDate(view.read());
        view.write("Enter project cost");
        int cost = Utilita.validateNumber(view.read());
        view.write("Enter project start date (format YYYY-MM-DD)");
        Timestamp startDate = Utilita.validateDate(view.read());
        Project project = new Project();
        project.setName(name);
        project.setReleaseDate(releaseDate);
        project.setCost(cost);
        project.setProjectStart(startDate);
        view.write("Creating project...");
        projectDAO.create(project);
    }
}
