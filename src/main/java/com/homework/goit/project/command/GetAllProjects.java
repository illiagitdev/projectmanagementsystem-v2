package com.homework.goit.project.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.project.Project;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

import java.util.List;
import java.util.Objects;

public class GetAllProjects implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public GetAllProjects(View view, ProjectDAOImpl projectDAO) {
        this.view = view;
        this.projectDAO = projectDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_ALL_PROJECTS;
    }

    @Override
    public void execute() {
        view.write("Retrieving all projects...");
        List<Project> projects = projectDAO.getAll();
        if (Objects.nonNull(projects)) {
            for (Project project : projects) {
                view.write(String.format("Project(%s):\n\t%s - %s $ - %s", project.getId(), project.getName(),
                        project.getCost(), project.getProjectStart()));
            }
        } else {
            view.write("No projects found.");
        }
    }
}
