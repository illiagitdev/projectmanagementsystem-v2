package com.homework.goit.project.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.project.ProjectDAOImpl;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectsInfo;

import java.util.List;

public class ShowProjectsInfo implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public ShowProjectsInfo(View view, ProjectDAOImpl projectDAO) {
        this.view = view;
        this.projectDAO = projectDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_PROJECT_INFO;
    }

    @Override
    public void execute() {
        view.write("Projects: start date / project name / developers in project");
        List<ProjectsInfo> projects = projectDAO.getProjects();
        for (ProjectsInfo project: projects) {
            view.write(String.format("%s / %S / %d", project.getStartDate(), project.getProjectName(),
                    project.getDevelopersInvolved()));
        }
    }
}
