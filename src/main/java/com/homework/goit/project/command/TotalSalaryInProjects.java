package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.project.ProjectDAOImpl;
import com.homework.goit.project.ProjectDAO;

import java.math.BigInteger;
import java.util.Map;

public class TotalSalaryInProjects implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public TotalSalaryInProjects(View view, ProjectDAOImpl projectDAO) {
        this.view = view;
        this.projectDAO = projectDAO;
    }

    @Override
    public String command() {
        return Commands.TOTAL_SALARY_IN_PROJECT;
    }

    @Override
    public void execute() {
        view.write("Enter project id to see total salary");
        int id = Utilita.validateNumber(view.read());
        view.write("Project id(" + id + "):");
        Map<String, BigInteger> projectSalary = projectDAO.getSalaryByProject(id);
        for (Map.Entry entry : projectSalary.entrySet()) {
            view.write(String.format("Project:\n\t%s -> %s $", entry.getKey(), entry.getValue()));
        }
    }
}
