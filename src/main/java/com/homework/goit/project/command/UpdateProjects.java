package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.project.Project;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

import java.sql.Timestamp;

public class UpdateProjects implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public UpdateProjects(View view, ProjectDAOImpl projectDAO) {
        this.view = view;
        this.projectDAO = projectDAO;
    }

    @Override
    public String command() {
        return Commands.UPDATE_PROJECT;
    }

    @Override
    public void execute() {
        view.write("Enter project id");
        int id = Utilita.validateNumber(view.read());
        view.write("Update project name");
        String name = Utilita.validate(view.read());
        view.write("Update release date (format YYYY-MM-DD)");
        Timestamp releaseDate = Utilita.validateDate(view.read());
        view.write("Update project cost");
        int cost = Utilita.validateNumber(view.read());
        view.write("Update project start date (format YYYY-MM-DD)");
        Timestamp startDate = Utilita.validateDate(view.read());
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setReleaseDate(releaseDate);
        project.setCost(cost);
        project.setProjectStart(startDate);
        view.write("Updating project...");
        try {
            projectDAO.update(project);
        } catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }
}
