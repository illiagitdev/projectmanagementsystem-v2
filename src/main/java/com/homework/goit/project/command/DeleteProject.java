package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

public class DeleteProject implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public DeleteProject(View view, ProjectDAOImpl projectDAO) {
        this.view = view;
        this.projectDAO = projectDAO;
    }

    @Override
    public String command() {
        return Commands.DELETE_PROJECT;
    }

    @Override
    public void execute() {
        view.write("Enter project id to delete");
        int id = Utilita.validateNumber(view.read());
        view.write("Deleting project...");
        try {
            projectDAO.delete(id);
        } catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }
}
