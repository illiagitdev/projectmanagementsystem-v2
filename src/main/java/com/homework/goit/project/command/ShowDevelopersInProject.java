package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.developer.Developer;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

import java.util.*;

public class ShowDevelopersInProject implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public ShowDevelopersInProject(View view, ProjectDAOImpl projectDAO) {
        this.view = view;
        this.projectDAO = projectDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_DEVELOPERS_IN_PROJECT;
    }

    @Override
    public void execute() {
        view.write("Enter project id to see it's developers");
        int id = Utilita.validateNumber(view.read());
        Map<String, List<Developer>> projectDevelopers = projectDAO.getDevelopersInProject(id);
        if (!projectDevelopers.isEmpty()) {
            projectDevelopers.forEach((name, devs) -> {
                view.write(name);
                devs.forEach(developer -> view.write(String.format("Developer:\n\t%s %s %s %s %s", developer.getFirstName(), developer.getLastName(),
                        developer.getAge(), developer.getEmail(), developer.getGender())));
            });
        } else {
            view.write("Developers with such skill not found!");
        }
    }
}
