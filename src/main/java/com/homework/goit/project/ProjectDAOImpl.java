package com.homework.goit.project;

import com.homework.goit.developer.Developer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigInteger;
import java.sql.*;
import java.time.LocalDate;
import java.util.*;

public class ProjectDAOImpl implements ProjectDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(ProjectDAOImpl.class);
    private static final String PROJECT_INFO = "SELECT pr.project_start, pr.name, count(d.id) FROM projects pr " +
            "JOIN developers_projects dp on pr.id = dp.project_id " +
            "JOIN developers d on dp.developer_id = d.id " +
            "GROUP BY pr.name, pr.project_start";
    private static final String PROJECT_BY_SALARY = "SELECT pr.name, sum(dev.salary) FROM developers dev " +
            "JOIN  developers_projects dev_p ON dev.id = dev_p.developer_id " +
            "JOIN projects pr ON pr.id = dev_p.project_id " +
            "WHERE pr.id=:id  GROUP BY pr.name";
    private SessionFactory sessionFactory;

    public ProjectDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Project project) {
        LOG.info(String.format("create: project='%s'", project.getName()));
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            session.save(project);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("create(project):error with name '%s'", project.getName()), e);
        }
    }

    @Override
    public void update(Project project) {
        LOG.info(String.format("update: project='%s'", project.getName()));
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            session.update(project);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("update(project):error with updating '%s'", project.getName()), e);
        }
    }

    @Override
    public Project get(int id) {
        Project project = null;
        LOG.info(String.format("get: id='%d'", id));
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            project = session.createQuery("from Project p where p.id=:id", Project.class)
                    .setParameter("id", id)
                   .uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("get(id):error with id '%s'", id), e);
        }
        return project;
    }

    @Override
    public List<Project> getAll() {
        List<Project> projects = new ArrayList<>();
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            projects = session.createQuery("from Project ", Project.class).getResultList();
            transaction.commit();
        } catch (Exception e) {
            LOG.error("getAll:error -> ", e);
        }
        return projects;
    }

    @Override
    public void delete(int id) {
        LOG.info(String.format("delete: id='%d'", id));
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            session.createSQLQuery("DELETE FROM customers_projects WHERE project_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createSQLQuery("DELETE FROM companies_projects WHERE project_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createSQLQuery("DELETE FROM developers_projects WHERE project_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createQuery("DELETE FROM Project WHERE id=:id").setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("delete(id):error with id '%s'", id), e);
        }
    }

    @Override
    public Map<String, BigInteger> getSalaryByProject(int id) {
        Map<String, BigInteger> result = new HashMap<>();
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            Object[] items  = (Object[]) session.createSQLQuery(PROJECT_BY_SALARY)
                    .setParameter("id", id)
                    .uniqueResult();
            String key = (String) items[0];
            BigInteger value = (BigInteger) items[1];
            result.put(key, value);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("getSalaryByProject(id):error with id '%s'", id), e);
        }
        return result;
    }

    @Override
    public List<ProjectsInfo> getProjects() {
        List<ProjectsInfo> projectsInfo = new ArrayList<>();
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            List infoList = session.createSQLQuery(PROJECT_INFO)
                    .list();

            ProjectsInfo projInfo;
            for (Object obj : infoList) {
                Object[] objects = (Object[]) obj;
                projInfo = new ProjectsInfo();
                projInfo.setStartDate(parseDate(objects[0]));
                projInfo.setProjectName((String) objects[1]);
                projInfo.setDevelopersInvolved((BigInteger) objects[2]);
                projectsInfo.add(projInfo);
            }
            transaction.commit();
        } catch (Exception e) {
            LOG.error("getProjects():error ", e);
        }
        return projectsInfo;
    }

    @Override
    public Map<String, List<Developer>> getDevelopersInProject(int id) {
        LOG.info(String.format("getDevelopersInProject(id): id='%d'", id));
        Map<String, List<Developer>> developers = new HashMap<>();
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            Project project = session.createQuery("from Project p where p.id=:id", Project.class)
                    .setParameter("id", id)
                    .uniqueResult();
            developers.put(project.getName(), project.getDevelopers());
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("getDevelopersInProject(id):error with id '%s'", id), e);
        }
        return  developers;
    }

    private LocalDate parseDate(Object object) {
        String parseValue = object.toString();
        if (parseValue.strip().isBlank()) {
            return null;
        }
        try {
            Timestamp timestamp = Timestamp.valueOf(parseValue);
            return timestamp.toLocalDateTime().toLocalDate();
        } catch (Exception e) {
            LOG.error(String.format("validateDate:error with value '%s'", parseValue), e);
        }
        return null;
    }
}
