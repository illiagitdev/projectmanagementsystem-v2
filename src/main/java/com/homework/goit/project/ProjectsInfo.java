package com.homework.goit.project;

import java.math.BigInteger;
import java.time.LocalDate;

public class ProjectsInfo {
    private LocalDate startDate;
    private String projectName;
    private BigInteger developersInvolved;

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public BigInteger getDevelopersInvolved() {
        return developersInvolved;
    }

    public void setDevelopersInvolved(BigInteger developersInvolved) {
        this.developersInvolved = developersInvolved;
    }
}
