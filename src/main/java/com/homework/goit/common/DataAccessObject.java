package com.homework.goit.common;

import java.util.List;

public interface DataAccessObject<T extends Entity> {
        void create(T t);
        void update(T t);
        T get(int id);
        List<T> getAll();
        void delete(int id);
}
