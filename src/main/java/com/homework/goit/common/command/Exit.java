package com.homework.goit.common.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.config.HibernateDatabaseConnector;

public class Exit implements Command {
    private View view;

    public Exit(View view) {
        this.view = view;
    }

    @Override
    public String command() {
        return Commands.EXIT;
    }

    @Override
    public void execute() {
        view.write("Bye!");
        HibernateDatabaseConnector.getInstance().destroy();
        System.exit(0);
    }
}
