package com.homework.goit.common.command.utils;

public class Commands {
    public static final String HELP = "help";
    public static final String EXIT = "exit";
    public static final String UPDATE_PROJECT = "update_project";
    public static final String TOTAL_SALARY_IN_PROJECT = "total_salary_in_project";
    public static final String SHOW_PROJECT_INFO = "show_projects_info";
    public static final String SHOW_PROJECT_BY_ID = "show_project_by_id";
    public static final String SHOW_ALL_PROJECTS = "show_all_projects";
    public static final String DELETE_PROJECT = "delete_project";
    public static final String CREATE_PROJECT = "create_project";
    public static final String SHOW_DEVELOPERS_IN_PROJECT = "show_developers_in_project";
    public static final String CREATE_CUSTOMER = "create_customer";
    public static final String UPDATE_CUSTOMER = "update_customer";
    public static final String SHOW_CUSTOMER_BY_ID = "show_customer_by_id";
    public static final String SHOW_ALL_CUSTOMERS = "show_all_customers";
    public static final String DELETE_CUSTOMER = "delete_customer";
    public static final String CREATE_COMPANY = "create_company";
    public static final String UPDATE_COMPANY = "update_company";
    public static final String SHOW_COMPANY_BY_ID = "show_company_by_id";
    public static final String SHOW_ALL_COMPANIES = "show_all_companies";
    public static final String DELETE_COMPANY = "delete_company";
    public static final String CREATE_DEVELOPER = "create_developer";
    public static final String UPDATE_DEVELOPER = "update_developer";
    public static final String SHOW_DEVELOPER_BY_ID = "show_developer_by_id";
    public static final String SHOW_ALL_DEVELOPERS = "show_all_developers";
    public static final String DELETE_DEVELOPER = "delete_developer";
    public static final String SHOW_DEVELOPERS_BY_SKILL = "show_java_developers";
    public static final String SHOW_DEVELOPERS_BY_SKILL_LEVEL = "show_middle_developers";
}
