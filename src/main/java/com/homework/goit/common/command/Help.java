package com.homework.goit.common.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.common.command.utils.Commands;

public class Help implements Command {
    private View view;

    public Help(View view) {
        this.view = view;
    }

    @Override
    public String command() {
        return Commands.HELP;
    }

    @Override
    public void execute() {
        view.write("------------------------------------------------------------------------------");
        view.write("-------------------------  Commands list  ------------------------------------");
        view.write("------------------------------------------------------------------------------");
        view.write("-\tCommand:                Description:");
        view.write(String.format("-\t%s                    - shows all available commands", Commands.HELP));
        view.write(String.format("-\t%s          - create new company", Commands.CREATE_COMPANY));
        view.write(String.format("-\t%s          - update existing company by id", Commands.UPDATE_COMPANY));
        view.write(String.format("-\t%s       - show company by id", Commands.SHOW_COMPANY_BY_ID));
        view.write(String.format("-\t%s       - show all companies", Commands.SHOW_ALL_COMPANIES));
        view.write(String.format("-\t%s          - delete company be id", Commands.DELETE_COMPANY));
        view.write("");
        view.write(String.format("-\t%s         - create new customer", Commands.CREATE_CUSTOMER));
        view.write(String.format("-\t%s         - update existing customer by id", Commands.UPDATE_CUSTOMER));
        view.write(String.format("-\t%s         - show customer by id", Commands.SHOW_CUSTOMER_BY_ID));
        view.write(String.format("-\t%s         - show all customers", Commands.SHOW_ALL_CUSTOMERS));
        view.write(String.format("-\t%s         - delete customer be id", Commands.DELETE_CUSTOMER));
        view.write("");
        view.write(String.format("-\t%s          - create new project", Commands.CREATE_PROJECT));
        view.write(String.format("-\t%s          - update existing project by id", Commands.UPDATE_PROJECT));
        view.write(String.format("-\t%s          - show project by id", Commands.SHOW_PROJECT_BY_ID));
        view.write(String.format("-\t%s          - show all projects", Commands.SHOW_ALL_PROJECTS));
        view.write(String.format("-\t%s          - delete project be id", Commands.DELETE_PROJECT));
        view.write("");
        view.write(String.format("-\t%s        - create new developer", Commands.CREATE_DEVELOPER));
        view.write(String.format("-\t%s        - update existing developer by id", Commands.UPDATE_DEVELOPER));
        view.write(String.format("-\t%s     - show developers by id", Commands.SHOW_DEVELOPER_BY_ID));
        view.write(String.format("-\t%s      - show all developers", Commands.SHOW_ALL_DEVELOPERS));
        view.write(String.format("-\t%s        - delete developer be id", Commands.DELETE_DEVELOPER));
        view.write("");
        view.write(String.format("-\t%s - show total developers salary in project with id", Commands.TOTAL_SALARY_IN_PROJECT));
        view.write(String.format("-\t%s      - show base projects information", Commands.SHOW_PROJECT_INFO));
        view.write("");
        view.write(String.format("-\t%s    - show list of all Java developers", Commands.SHOW_DEVELOPERS_BY_SKILL));
        view.write(String.format("-\t%s  - show list of all middle developers", Commands.SHOW_DEVELOPERS_BY_SKILL_LEVEL));
        view.write(String.format("-\t%s - show list of developers in project with id", Commands.SHOW_DEVELOPERS_IN_PROJECT));
        view.write("");
        view.write(String.format("-\t%s                     - stop application", Commands.EXIT));
        view.write("------------------------------------------------------------------------------");
        view.write(" ");
    }
}
