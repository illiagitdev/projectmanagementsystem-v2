package com.homework.goit.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilita {
    private static final Logger LOG = LogManager.getFormatterLogger(Utilita.class);
    private static View view = new Console();

    public static int validateNumber(String value) {
        LOG.debug(String.format("validateNumber: value='%s'", value));
        int res = 0;
        try {
            res = Integer.parseInt(value);
        } catch (RuntimeException e) {
            LOG.error(String.format("validateNumber:error with value '%s'", value), e);
            view.write("Input not a number!");
        }
        return res;
    }

    public static String validate(String value) {
        LOG.debug(String.format("validate: value='%s'", value));
        while (value.strip().isEmpty()) {
            view.write("Field can't be empty");
            value = view.read();
        }
        return value;
    }

    public static Timestamp validateDate(String value) {
        LOG.debug(String.format("validateDate: value='%s'", value));
        if (value.strip().isBlank()) {
            return null;
        }
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-d");

            Date parseDate = formatter.parse(value);
            return new Timestamp(parseDate.getTime());
        } catch (Exception e) {
            LOG.error(String.format("validateDate:error with value '%s'", value), e);
            view.write("Wrong date format!");
        }
        return null;
    }
}
