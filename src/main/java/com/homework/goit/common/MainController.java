package com.homework.goit.common;

import com.homework.goit.common.command.Exit;
import com.homework.goit.common.command.Help;
import com.homework.goit.company.CompanyDAO;
import com.homework.goit.company.CompanyDAOImpl;
import com.homework.goit.company.command.*;
import com.homework.goit.customer.CustomerDAOImpl;
import com.homework.goit.customer.command.*;
import com.homework.goit.developer.DeveloperDAOImpl;
import com.homework.goit.developer.command.*;
import com.homework.goit.project.ProjectDAOImpl;
import com.homework.goit.project.command.*;
import com.homework.goit.skill.SkillDAOImpl;
import com.homework.goit.skill.command.ShowDevelopersBySkill;
import com.homework.goit.skill.command.ShowMiddleDevelopers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;

import java.util.Arrays;
import java.util.List;

public class MainController {
    private static final Logger LOG = LogManager.getFormatterLogger(MainController.class);
    private View view;
    private List<Command> commands;

    public MainController(View view, SessionFactory sessionFactory) {
        this.view = view;
        final CompanyDAO companyDAO = new CompanyDAOImpl(sessionFactory);
        final CustomerDAOImpl customerDAO = new CustomerDAOImpl(sessionFactory);
        final ProjectDAOImpl projectDAO = new ProjectDAOImpl(sessionFactory);
        final DeveloperDAOImpl developerDAO = new DeveloperDAOImpl(sessionFactory);
        final SkillDAOImpl skillDAO = new SkillDAOImpl(sessionFactory);
        commands = Arrays.asList(
                new Help(view),
                new CreateCompany(view, companyDAO),
                new UpdateCompany(view, companyDAO),
                new GetCompanyById(view, companyDAO),
                new GetAllCompanies(view, companyDAO),
                new DeleteCompany(view, companyDAO),

                new CreateCustomer(view, customerDAO),
                new UpdateCustomer(view, customerDAO),
                new GetCustomerById(view, customerDAO),
                new GetAllCustomers(view, customerDAO),
                new DeleteCustomer(view, customerDAO),

                new CreateProjects(view, projectDAO),
                new UpdateProjects(view, projectDAO),
                new GetProjectById(view, projectDAO),
                new GetAllProjects(view, projectDAO),
                new DeleteProject(view, projectDAO),
                new TotalSalaryInProjects(view, projectDAO),
                new ShowProjectsInfo(view, projectDAO),
                new ShowDevelopersInProject(view, projectDAO),

                new CreateDeveloper(view, developerDAO),
                new UpdateDeveloper(view, developerDAO),
                new GetDeveloperById(view, developerDAO),
                new GetAllDevelopers(view, developerDAO),
                new DeleteDeveloper(view, developerDAO),

                new ShowDevelopersBySkill(view, skillDAO),
                new ShowMiddleDevelopers(view, skillDAO),

                new Exit(view)
        );
    }

    public void process(){
        view.write("Welcome to Project Management System!");
        while (true){
            view.write("Enter commands ('help' to see list of commands)!)");
            String actions = view.read();
            doCommand(actions);
        }
    }

    private void doCommand(String actions) {
        LOG.debug(String.format("doCommand:action=%s", actions));
        for (Command command: commands) {
            if (command.canExecute(actions)){
                command.execute();
                break;
            }
        }
    }
}
