package com.homework.goit.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateDatabaseConnector {
    private static final Logger LOG = LogManager.getLogger(HibernateDatabaseConnector.class);
    private static StandardServiceRegistry registry;
    private static SessionFactory sessionFactory;
    private static HibernateDatabaseConnector instance;

    static {
        instance = new HibernateDatabaseConnector();
    }

    private HibernateDatabaseConnector() {
        init();
    }

    public static HibernateDatabaseConnector getInstance() {
        return instance;
    }

    private synchronized void init () {
        try {
            registry = new StandardServiceRegistryBuilder().configure().build();
            MetadataSources metadataSources = new MetadataSources(registry);
            final Metadata metadata = metadataSources.getMetadataBuilder().build();
            sessionFactory = metadata.getSessionFactoryBuilder().build();
        } catch (Exception e) {
            LOG.error("init error", e);
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public synchronized void destroy() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }
}
