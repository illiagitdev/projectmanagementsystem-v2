package com.homework.goit.skill;

import com.homework.goit.common.DataAccessObject;

import java.util.List;

public interface SkillDAO extends DataAccessObject<Skill> {
    List<Skill> getDeveloperBySkill(String skill);
    List<Skill> getDevelopersBySkillLevel(String skillLevel);
}
