package com.homework.goit.skill;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class SkillDAOImpl implements SkillDAO {
    private static final Logger LOG = LogManager.getLogger(SkillDAOImpl.class);
    private SessionFactory sessionFactory;

    public SkillDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Skill skill) {
        LOG.debug(String.format("create: %s", skill.getSkill()));
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            session.save(skill);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("error: -> create: %s", skill.getSkill()), e);
        }
    }

    @Override
    public void update(Skill skill) {
        LOG.debug(String.format("update: %s", skill.getSkill()));
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            session.update(skill);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("error: -> update: %s", skill.getSkill()), e);
        }
    }

    @Override
    public Skill get(int id) {
        LOG.debug(String.format("get: %s", id));
        Skill skill = null;
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            skill = session.createQuery("from Skill s where s.id=:id", Skill.class)
                    .setParameter("id", id)
                    .uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("error: -> get: %s", id), e);
        }
        return skill;
    }

    @Override
    public List<Skill> getAll() {
        List<Skill> skills = null;
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            skills = session.createQuery("from Skill", Skill.class).getResultList();
            transaction.commit();
        } catch (Exception e) {
            LOG.error("error: -> getAll: ", e);
        }
        return skills;
    }

    @Override
    public void delete(int id) {
        LOG.debug(String.format("delete: %s", id));
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            session.createSQLQuery("DELETE FROM developers_skills WHERE skill_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createQuery("DELETE FROM Skill WHERE id=:id").setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("error: -> delete: %s", id), e);
        }
    }

    @Override
    public List<Skill> getDeveloperBySkill(String skill) {
        List<Skill> skills = new ArrayList<>();
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            skills = session.createQuery("from Skill s where s.skill=:skill", Skill.class)
                    .setParameter("skill", skill)
                    .getResultList();
            transaction.commit();
        } catch (Exception e) {
            LOG.error("getJavaDeveloper():error -> '", e);
        }
        return skills;
    }

    @Override
    public List<Skill> getDevelopersBySkillLevel(String skillLevel) {
        List<Skill> skills = new ArrayList<>();
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            skills = session.createQuery("from Skill s where s.level=:skillLevel", Skill.class)
                    .setParameter("skillLevel", skillLevel)
                    .getResultList();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("getDevelopersBySkillLevel:error with skill level '%s'", skillLevel), e);
        }
        return skills;
    }
}
