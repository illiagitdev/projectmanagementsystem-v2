package com.homework.goit.skill;

import com.homework.goit.common.Entity;
import com.homework.goit.developer.Developer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Skill extends Entity {
    private String skill;
    private String level;
    private List<Developer> developers = new ArrayList<>();

    public Skill() {
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Skill skill1 = (Skill) o;
        return Objects.equals(skill, skill1.skill) &&
                Objects.equals(level, skill1.level);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), skill, level);
    }
}
