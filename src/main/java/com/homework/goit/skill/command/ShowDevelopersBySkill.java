package com.homework.goit.skill.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.skill.Skill;
import com.homework.goit.skill.SkillDAO;
import com.homework.goit.skill.SkillDAOImpl;

import java.util.List;

public class ShowDevelopersBySkill implements Command {
    private static final String DEFAULT_SKILL = "Java";
    private View view;
    private SkillDAO skillDAO;

    public ShowDevelopersBySkill(View view, SkillDAOImpl skillDAO) {
        this.view = view;
        this.skillDAO = skillDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_DEVELOPERS_BY_SKILL;
    }

    @Override
    public void execute() {
        List<Skill> skills = skillDAO.getDeveloperBySkill(DEFAULT_SKILL);
        if (!skills.isEmpty()) {
            view.write(String.format("All %s developers:", DEFAULT_SKILL));
            for (Skill skill: skills) {
                view.write(String.format("%s (%s)", skill.getSkill(), skill.getLevel()));
                skill.getDevelopers().forEach(developer -> view.write(String.format("Developer:\n\t%s %s %s %s %s",
                        developer.getFirstName(), developer.getLastName(),
                        developer.getAge(), developer.getEmail(), developer.getGender())));
            }
        } else {
            view.write(String.format("Developers with skill: %s not found!", DEFAULT_SKILL));
        }
    }
}
