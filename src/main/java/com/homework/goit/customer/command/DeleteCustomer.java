package com.homework.goit.customer.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

public class DeleteCustomer implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public DeleteCustomer(View view, CustomerDAOImpl customerDAO) {
        this.view = view;
        this.customerDAO = customerDAO;
    }

    @Override
    public String command() {
        return Commands.DELETE_CUSTOMER;
    }

    @Override
    public void execute() {
        view.write("Enter customer id to delete");
        int id = Utilita.validateNumber(view.read());
        view.write("Deleting customer...");
        customerDAO.delete(id);
    }
}
