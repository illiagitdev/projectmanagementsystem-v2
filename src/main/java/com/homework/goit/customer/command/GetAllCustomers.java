package com.homework.goit.customer.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.DataAccessObject;
import com.homework.goit.common.View;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

import java.util.List;

public class GetAllCustomers implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public GetAllCustomers(View view, CustomerDAOImpl customerDAO) {
        this.view = view;
        this.customerDAO = customerDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_ALL_CUSTOMERS;
    }

    @Override
    public void execute() {
        view.write("Retrieving all customers...");
        List<Customer> customers = customerDAO.getAll();
        if (!customers.isEmpty()) {
            for (Customer customer : customers) {
                view.write(String.format("Customer(%s):\n\t%s - %s", customer.getId(), customer.getName(),
                        customer.getBudget()));
            }
        } else {
            view.write("No customers found.");
        }
    }
}
