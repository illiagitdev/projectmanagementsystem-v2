package com.homework.goit.customer.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

public class CreateCustomer implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public CreateCustomer(View view, CustomerDAOImpl customerDAO) {
        this.view = view;
        this.customerDAO = customerDAO;
    }

    @Override
    public String command() {
        return Commands.CREATE_CUSTOMER;
    }

    @Override
    public void execute() {
        view.write("Enter customer name");
        String name = Utilita.validate(view.read());
        view.write("Enter customer budget");
        int budget = Utilita.validateNumber(view.read());
        view.write("Creating customer...");
        Customer customer = new Customer();
        customer.setName(name);
        customer.setBudget(budget);
        customerDAO.create(customer);
    }
}
