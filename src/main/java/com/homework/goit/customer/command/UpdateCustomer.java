package com.homework.goit.customer.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

public class UpdateCustomer implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public UpdateCustomer(View view, CustomerDAOImpl customerDAO) {
        this.view = view;
        this.customerDAO = customerDAO;
    }

    @Override
    public String command() {
        return Commands.UPDATE_CUSTOMER;
    }

    @Override
    public void execute() {
        view.write("Enter customer id for update");
        int id = Utilita.validateNumber(view.read());
        view.write("Update customer name");
        String name = Utilita.validate(view.read());
        view.write("Update customer budget");
        int budget = Utilita.validateNumber(view.read());
        Customer customer = new Customer();
        customer.setId(id);
        customer.setName(name);
        customer.setBudget(budget);
        view.write("Updating customer...");
        customerDAO.update(customer);
    }
}
