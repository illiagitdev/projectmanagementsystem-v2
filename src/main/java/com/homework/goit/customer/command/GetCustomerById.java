package com.homework.goit.customer.command;

import com.homework.goit.common.*;
import com.homework.goit.common.command.utils.Commands;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

public class GetCustomerById implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public GetCustomerById(View view, CustomerDAOImpl customerDAO) {
        this.view = view;
        this.customerDAO = customerDAO;
    }

    @Override
    public String command() {
        return Commands.SHOW_CUSTOMER_BY_ID;
    }

    @Override
    public void execute() {
        view.write("Enter customer id");
        int id = Utilita.validateNumber(view.read());
        view.write("Searching customer...");
        Customer customer = customerDAO.get(id);
        if (customer != null) {
            view.write(String.format("Customer:\n\t%s - %s", customer.getName(), customer.getBudget()));
        } else {
            view.write("Customer with such id were not found!");
        }
    }
}
