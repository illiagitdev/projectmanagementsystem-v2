package com.homework.goit.customer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(CustomerDAOImpl.class);
    private SessionFactory sessionFactory;

    public CustomerDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Customer customer) {
        LOG.info(String.format("create: customer='%s'", customer.getName()));
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            session.save(customer);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("create(customer):error create '%s'", customer.getName()), e);
        }
    }

    @Override
    public void update(Customer customer) {
        LOG.info(String.format("update: customer='%s'", customer.getName()));
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            session.update(customer);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("update(customer):error update '%s'", customer.getName()), e);
        }
    }

    @Override
    public Customer get(int id) {
        LOG.info(String.format("get: id='%d'", id));
        Customer customer = null;
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            customer = session.createQuery("from Customer c where c.id=:id", Customer.class)
                    .setParameter("id", id)
                    .uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("get(id):error with id '%s'", id), e);
        }
        return customer;
    }

    @Override
    public List<Customer> getAll() {
        List<Customer> customerList = new ArrayList<>();
        try (final Session session = sessionFactory.openSession()){
            final Transaction transaction = session.beginTransaction();
            customerList = session.createQuery("from Customer ", Customer.class).getResultList();
            transaction.commit();
        } catch (Exception e) {
            LOG.error("getAll:error -> ", e);
        }
        return customerList;
    }

    @Override
    public void delete(int id) {
        LOG.info(String.format("delete: id='%d'", id));
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();
            session.createSQLQuery("DELETE FROM customers_projects  WHERE customer_id=:id")
                    .setParameter("id", id).executeUpdate();
            session.createQuery("DELETE FROM Customer WHERE id=:id").setParameter("id", id).executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("delete(id):error with id '%s'", id), e);
        }
    }
}
