package com.homework.goit.customer;

import com.homework.goit.common.DataAccessObject;

public interface CustomerDAO extends DataAccessObject<Customer> {
}
