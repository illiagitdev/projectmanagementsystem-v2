INSERT INTO developers (first_name, last_name, age, email, sex) VALUES ('Mark', 'Greenhorn', 41, 'werGren@mail.com', 'MALE');
INSERT INTO developers (first_name, last_name, age, email, sex) VALUES ('Rosa', 'Green', 34, 'rose@mail.com', 'FEMALE');
INSERT INTO developers (first_name, last_name, age, email, sex) VALUES ('Kate', 'Tomson', 25, 'kate@mail.com', 'FEMALE');
INSERT INTO developers (first_name, last_name, age, email, sex) VALUES ('Rayan', 'McDan', 42, 'rayan@mail.com', 'MALE');
INSERT INTO developers (first_name, last_name, age, email, sex) VALUES ('Tom', 'Gray', 39, 'tom@mail.com', 'MALE');
INSERT INTO developers (first_name, last_name, age, email, sex) VALUES ('Kelly', 'Osborn', 28, 'kelly@mail.com', 'FEMALE');
INSERT INTO developers (first_name, last_name, age, email, sex) VALUES ('Richard', 'Patrik', 47, 'richard@mail.com', 'MALE');

INSERT INTO skills (skill, level) VALUES ('Java', 'Junior');
INSERT INTO skills (skill, level) VALUES ('Java', 'Middle');
INSERT INTO skills (skill, level) VALUES ('Java', 'Senior');
INSERT INTO skills (skill, level) VALUES ('C++', 'Junior');
INSERT INTO skills (skill, level) VALUES ('C++', 'Middle');
INSERT INTO skills (skill, level) VALUES ('C++', 'Senior');
INSERT INTO skills (skill, level) VALUES ('C#', 'Junior');
INSERT INTO skills (skill, level) VALUES ('C#', 'Middle');
INSERT INTO skills (skill, level) VALUES ('C#', 'Senior');
INSERT INTO skills (skill, level) VALUES ('JS', 'Junior');
INSERT INTO skills (skill, level) VALUES ('JS', 'Middle');
INSERT INTO skills (skill, level) VALUES ('JS', 'Senior');

INSERT INTO developers_skills (developer_id, skill_id) VALUES (1, 1);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (1, 5);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (1, 11);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (2, 2);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (2, 4);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (2, 11);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (3, 1);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (3, 10);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (4, 9);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (4, 12);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (5, 5);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (5, 8);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (5, 12);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (6, 2);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (6, 4);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (6, 10);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (7, 3);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (7, 6);
INSERT INTO developers_skills (developer_id, skill_id) VALUES (7, 11);

INSERT INTO projects (name, release_date) VALUES ('Video Trecker', '2020-8-17 00:00:00+00');
INSERT INTO projects (name, release_date) VALUES ('Star Cloud Storage', '2022-1-11 00:00:00+00');
INSERT INTO projects (name, release_date) VALUES ('Banc Accountant', '2021-3-27 00:00:00+00');
INSERT INTO projects (name, release_date) VALUES ('Emterprise Billing Manager', '2021-9-5 00:00:00+00');
INSERT INTO projects (name, release_date) VALUES ('CMS for Prophy', '2020-11-7 00:00:00+00');

INSERT INTO developers_projects (developer_id, project_id) VALUES (1, 2);
INSERT INTO developers_projects (developer_id, project_id) VALUES (1, 4);
INSERT INTO developers_projects (developer_id, project_id) VALUES (2, 1);
INSERT INTO developers_projects (developer_id, project_id) VALUES (2, 5);
INSERT INTO developers_projects (developer_id, project_id) VALUES (3, 4);
INSERT INTO developers_projects (developer_id, project_id) VALUES (4, 3);
INSERT INTO developers_projects (developer_id, project_id) VALUES (5, 3);
INSERT INTO developers_projects (developer_id, project_id) VALUES (6, 1);
INSERT INTO developers_projects (developer_id, project_id) VALUES (7, 2);
INSERT INTO developers_projects (developer_id, project_id) VALUES (7, 4);

INSERT INTO companies (name, location) VALUES ('Goldby', 'Kyiv');
INSERT INTO companies (name, location) VALUES ('Rest', 'Vinnitsa');
INSERT INTO companies (name, location) VALUES ('Star', 'Lvov');

INSERT INTO companies_projects (company_id, project_id) VALUES (1, 2);
INSERT INTO companies_projects (company_id, project_id) VALUES (1, 4);
INSERT INTO companies_projects (company_id, project_id) VALUES (2, 1);
INSERT INTO companies_projects (company_id, project_id) VALUES (2, 5);
INSERT INTO companies_projects (company_id, project_id) VALUES (3, 3);

INSERT INTO developers_companies (developer_id, company_id) VALUES (1, 1);
INSERT INTO developers_companies (developer_id, company_id) VALUES (2, 1);
INSERT INTO developers_companies (developer_id, company_id) VALUES (3, 3);
INSERT INTO developers_companies (developer_id, company_id) VALUES (4, 2);
INSERT INTO developers_companies (developer_id, company_id) VALUES (5, 1);
INSERT INTO developers_companies (developer_id, company_id) VALUES (6, 2);
INSERT INTO developers_companies (developer_id, company_id) VALUES (7, 3);

INSERT INTO customers (name, budget) VALUES ('Thomas', 450000);
INSERT INTO customers (name, budget) VALUES ('Merphy', 40000);
INSERT INTO customers (name, budget) VALUES ('Jonny', 280000);
INSERT INTO customers (name, budget) VALUES ('Albert', 340000);

INSERT INTO customers_projects (customer_id, project_id) VALUES (1, 2);
INSERT INTO customers_projects (customer_id, project_id) VALUES (1, 3);
INSERT INTO customers_projects (customer_id, project_id) VALUES (2, 1);
INSERT INTO customers_projects (customer_id, project_id) VALUES (3, 2);
INSERT INTO customers_projects (customer_id, project_id) VALUES (3, 4);
INSERT INTO customers_projects (customer_id, project_id) VALUES (4, 3);
INSERT INTO customers_projects (customer_id, project_id) VALUES (4, 4);

--update due to interaction by task 2
update developers set salary = 1600 where id = 1;
update developers set salary = 1800 where id = 2;
update developers set salary = 1100 where id = 3;
update developers set salary = 2700 where id = 4;
update developers set salary = 2900 where id = 5;
update developers set salary = 1400 where id = 6;
update developers set salary = 3200 where id = 7;

update projects set cost = 40000 where id =1;
update projects set cost = 258000 where id =2;
update projects set cost = 237000 where id =3;
update projects set cost = 547000 where id =4;
update projects set cost = 112000 where id =5;


--ALTER projects due to task with JDBC
UPDATE projects SET project_start = '2018-08-07 00:00:00+00' WHERE id = 1;
UPDATE projects SET project_start = '2017-01-11 00:00:00+00' WHERE id = 2;
UPDATE projects SET project_start = '2019-03-27 00:00:00+00' WHERE id = 3;
UPDATE projects SET project_start = '2019-09-05 00:00:00+00' WHERE id = 4;
UPDATE projects SET project_start = '2018-11-07 00:00:00+00' WHERE id = 5;
