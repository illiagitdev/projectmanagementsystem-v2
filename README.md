***Project Management System***

##Features
1. Developers
2. Companies
3. Projects
4. Clients

##Technologies

- Java Core
- Gradle
- Hibernate, ORM
- PostgreSQL
- PackageByFeature, DAO, DTO, Command
